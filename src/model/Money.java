package model;

public class Money {
	private double balance;
	private String name;
	
	
	
	public Money() {
		super();
	}

	

	public Money(double balance) {
		super();
		setBalance(balance);
	}



	public Money(double balance, String name) {
		super();
		setBalance(balance);
		setName(name);
	}



	public double getBalance() {
		return balance;
	}



	private void setBalance(double balance) {
		this.balance = balance;
	}

	public void deposit(double amount) {
		setBalance(getBalance()+amount);
	}
	
	public void withdraw(double amount) {
		setBalance(getBalance()-amount);
	}

	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String toString() {
		return name + "  " + balance;
	}
	
	
	
}
